/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;


import utilitaire.Modelview;
import utilitaire.Annotation;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.util.HashMap;
import java.util.Vector;

/**
 *
 * @author P14A-GOLD
 */
public class EmpListe {
    String Name;
    int Age;
    Date Date;

    public Date getDate() {
        return Date;
    }

    public void setDate(Date Date) {
        this.Date = Date;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public int getAge() {
        return Age;
    }

    public void setAge(int Age) {
        this.Age = Age;
    }
    
    @Annotation(name = "emplist")
    public Modelview test() {
        Modelview modelview = new Modelview();
        modelview.setUrlRequest("Emp.jsp");
        String a = "ssss test test";
        modelview.getData().put("test",a);
        return modelview;
    }
}
